package com.bbva.pzic.servicio_ejemplo.dao;

import com.bbva.pzic.servicio_ejemplo.business.dto.InputListProcessTasksProposals;
import com.bbva.pzic.servicio_ejemplo.facade.dto.ProcessTasks;

public interface IServicioEjemploDAO {
    ProcessTasks ListProcessTasksProposals(InputListProcessTasksProposals input);
}
