package com.bbva.pzic.servicio_ejemplo.dao.apx.mapper;

import com.bbva.pzic.servicio_ejemplo.business.dto.InputListProcessTasksProposals;
import com.bbva.pzic.servicio_ejemplo.dao.model.ppcutge1_1.PeticionTransaccionPpcutge1_1;
import com.bbva.pzic.servicio_ejemplo.dao.model.ppcutge1_1.RespuestaTransaccionPpcutge1_1;
import com.bbva.pzic.servicio_ejemplo.facade.dto.ProcessTasks;
import com.bbva.pzic.servicio_ejemplo.facade.dto.Status;

public interface IApxListProcessTasksProposalsMapper {

    PeticionTransaccionPpcutge1_1 mapIn(InputListProcessTasksProposals input);

    ProcessTasks mapOut(RespuestaTransaccionPpcutge1_1 respuesta);

    //Status mapOutStatus(com.bbva.pzic.servicio_ejemplo.dao.model.ppcutge1_1.Status status);
}