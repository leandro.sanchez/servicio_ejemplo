package com.bbva.pzic.servicio_ejemplo.dao.impl;

import com.bbva.pzic.servicio_ejemplo.business.dto.InputListProcessTasksProposals;
import com.bbva.pzic.servicio_ejemplo.dao.apx.ApxListProcessTasksProposals;
import com.bbva.pzic.servicio_ejemplo.dao.IServicioEjemploDAO;
import com.bbva.pzic.servicio_ejemplo.facade.dto.ProcessTasks;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServicioEjemploDAO implements IServicioEjemploDAO {
    private static final Log LOG = LogFactory.getLog(ServicioEjemploDAO.class);

    @Autowired
    private ApxListProcessTasksProposals apxListProcessTasksProposals;

    @Override
    public ProcessTasks ListProcessTasksProposals(InputListProcessTasksProposals input) {
        LOG.info("... Invoking method ServicioEjemploDAO.listProcessTasksProposals APX ...");

        return apxListProcessTasksProposals.perform(input);
    }
}
