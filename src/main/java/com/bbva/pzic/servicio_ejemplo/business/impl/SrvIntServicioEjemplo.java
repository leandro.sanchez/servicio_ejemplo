package com.bbva.pzic.servicio_ejemplo.business.impl;

import com.bbva.pzic.servicio_ejemplo.business.ISrvIntServicioEjemplo;
import com.bbva.pzic.servicio_ejemplo.business.dto.InputListProcessTasksProposals;
import com.bbva.pzic.servicio_ejemplo.dao.IServicioEjemploDAO;
import com.bbva.pzic.servicio_ejemplo.dao.impl.ServicioEjemploDAO;
import com.bbva.pzic.servicio_ejemplo.facade.dto.ProcessTasks;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SrvIntServicioEjemplo implements ISrvIntServicioEjemplo {
    private static final Log LOG = LogFactory.getLog(SrvIntServicioEjemplo.class);

    @Autowired
    private IServicioEjemploDAO servicioEjemploDAO;

    @Override
    public ProcessTasks ListProcessTasksProposals(final InputListProcessTasksProposals input) {
        LOG.info("... Invoking method SrvIntServicioEjemplo.listProcessTasksProposals APX ...");

        return servicioEjemploDAO.ListProcessTasksProposals(input);
    }
}
