package com.bbva.pzic.servicio_ejemplo.business;

import com.bbva.pzic.servicio_ejemplo.business.dto.InputListProcessTasksProposals;
import com.bbva.pzic.servicio_ejemplo.facade.dto.ProcessTasks;

public interface ISrvIntServicioEjemplo {
    ProcessTasks ListProcessTasksProposals(InputListProcessTasksProposals input);
}
