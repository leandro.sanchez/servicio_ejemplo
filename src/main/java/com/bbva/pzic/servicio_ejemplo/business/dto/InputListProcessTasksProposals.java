package com.bbva.pzic.servicio_ejemplo.business.dto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 13/04/2017.
 *
 * @author Entelgy
 */
public class InputListProcessTasksProposals {
    private String businessProcessId;
    private String taskId;

    public String getBusinessProcessId() {
        return businessProcessId;
    }

    public void setBusinessProcessId(String businessProcessId) {
        this.businessProcessId = businessProcessId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }
}