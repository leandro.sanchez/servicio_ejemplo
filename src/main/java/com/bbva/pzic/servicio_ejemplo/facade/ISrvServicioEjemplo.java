package com.bbva.pzic.servicio_ejemplo.facade;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.servicio_ejemplo.facade.dto.ProcessTasks;

public interface ISrvServicioEjemplo {
    ServiceResponse<ProcessTasks> listProcessTasksProposals(String businessProcessId, String taskId);
}
