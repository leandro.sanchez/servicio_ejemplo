package com.bbva.pzic.servicio_ejemplo.facade.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.servicio_ejemplo.business.dto.InputListProcessTasksProposals;
import com.bbva.pzic.servicio_ejemplo.facade.dto.ProcessTasks;

public interface IListProcessTasksProposalsMapper {

    InputListProcessTasksProposals mapIn(String businessProcessId, String taskId);

    ServiceResponse<ProcessTasks> mapOut(ProcessTasks processTasks);
}