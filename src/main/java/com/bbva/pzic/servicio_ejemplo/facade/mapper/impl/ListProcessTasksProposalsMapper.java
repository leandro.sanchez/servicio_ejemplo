package com.bbva.pzic.servicio_ejemplo.facade.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.servicio_ejemplo.business.dto.InputListProcessTasksProposals;
import com.bbva.pzic.servicio_ejemplo.facade.mapper.IListProcessTasksProposalsMapper;
import com.bbva.pzic.servicio_ejemplo.facade.dto.ProcessTasks;
import org.springframework.stereotype.Component;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


@Component
public class ListProcessTasksProposalsMapper implements IListProcessTasksProposalsMapper {
    private static final Log LOG = LogFactory.getLog(ListProcessTasksProposalsMapper.class);

    @Override
    public InputListProcessTasksProposals mapIn(final String businessProcessId, final String taskId) {
        LOG.info("... called method ListProcessTasksProposalsMapper.mapIn ...");

        InputListProcessTasksProposals dtoIn = new InputListProcessTasksProposals();
        dtoIn.setBusinessProcessId(businessProcessId);
        dtoIn.setTaskId(taskId);
        return dtoIn;
    }

    @Override
    public ServiceResponse<ProcessTasks> mapOut(final ProcessTasks processTasks) {
        LOG.info("... called method ListProcessTasksProposalsMapper.mapOut ...");

        if (processTasks==null){
            return null;
        }

        return ServiceResponse.data(processTasks).build();
    }
}
